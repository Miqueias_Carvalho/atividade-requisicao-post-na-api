import "./App.css";
import Login from "./components/Login";
import Display from "./components/Display";
import { useState } from "react";

function App() {
  const [isValid, setIsValid] = useState(false);
  const [showDisplay, setShowDisplay] = useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <Login setShowDisplay={setShowDisplay} setIsValid={setIsValid} />
        {showDisplay && <Display isValid={isValid} />}
      </header>
    </div>
  );
}

export default App;
