import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { TextField, Button } from "@material-ui/core";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";

// estilzação dos componentes
//=====================================================
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "yellow",
    width: "300px",
    padding: "30px",
    border: "4px solid white",
  },
}));

//função JSX
//======================================================
function Login({ setShowDisplay, setIsValid }) {
  //funções de validação do yup
  //-------------------------------------------
  const schema = yup.object().shape({
    username: yup.string().required("Campo Obrigatório"),
    password: yup
      .string()
      .required("Campo Obrigatório")
      .min(3, "Mínimo de 3 dígitos"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  //função de submit do Form
  //----------------------------------------------
  const onSubmitFunction = (data) => {
    console.log(data);

    axios
      .post("https://kenzieshop.herokuapp.com/sessions/", { ...data })
      .then((response) => {
        console.log(response);
        setIsValid(true);
        return response;
      })
      .catch((err) => {
        console.log(err);
        setIsValid(false);
      })
      .then(() => setShowDisplay(true));
  };

  //return do JSX
  //===================================================
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <div>
          <TextField
            id="outlined-basic"
            label="User Name"
            variant="outlined"
            size="medium"
            required
            {...register("username")}
            error={!!errors.name}
          />
        </div>
        <div>
          <TextField
            id="outlined-basic"
            margin="normal"
            label="Senha"
            variant="outlined"
            size="small"
            {...register("password")}
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        </div>
        <div>
          <Button
            type="submit"
            size="medium"
            variant="contained"
            color="primary"
          >
            Login
          </Button>
        </div>
      </form>
    </div>
  );
}
export default Login;
