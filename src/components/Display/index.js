import React from "react";
import { Alert } from "@material-ui/lab";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  margens: {
    margin: "10px",
  },
}));

function Display({ isValid }) {
  const classes = useStyles();
  return (
    <>
      <div className={classes.margens}>
        {isValid ? (
          <Alert variant="filled" severity="success">
            Requisição Completa!!
          </Alert>
        ) : (
          <Alert variant="filled" severity="error">
            Requisição Falhou!!
          </Alert>
        )}
      </div>
    </>
  );
}
export default Display;
